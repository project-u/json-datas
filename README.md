# project-u/json-datas
This repository provides a basic dataset of all regions, locations and areas (except for the 8th Generation game), and Pokémons Spawning Infos.

## Copyright Notice
Please note everything in repository are copyrighted by the Pokémon Company and its affiliates. This repository is merely a compilation of data collected by the editors of [Project-U](http://vytalic.fr/).